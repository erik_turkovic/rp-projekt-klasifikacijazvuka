import tensorflow as tf
import cv2
import numpy as np
import matplotlib.pyplot as plt
import librosa
import librosa.display
import warnings
from tkinter import *
from tkinter import filedialog
from PIL import ImageTk,Image
warnings.filterwarnings('ignore')

model = tf.keras.models.load_model('CNNFINALMODEL.model')

window=Tk()
window.geometry("900x800")
window['background']='white'
window.title("Klasifikacija")

slike = 'Data/images_original/'

CATEGORIES =["blues","classical","country","disco","hiphop","jazz","metal","pop","reggae","rock"]

IMG_SIZE = 224
n_fft = 8048
hop_length = 90
general_path = 'Data'
test_slika="test_slika.png"

def klikzvuk():
    global kugath
    global pozicija
    global img
    global DB
    global y, sr, audio_file, D, fig
    global ax, D_highres, S_db_hr
    global label, image_label
    global printed
    window.pozzvuka = filedialog.askopenfilename(initialdir="Data/MojiPrimjeri", title="Odaberi audio",filetypes=(("wav files","*.wav"),))
    kugath = window.pozzvuka
    y, sr = librosa.load(kugath)
    audio_file, _ = librosa.effects.trim(y)
    D = np.abs(librosa.stft(audio_file, n_fft=n_fft, hop_length=hop_length))
    DB = librosa.amplitude_to_db(D, ref=np.max)
    fig, ax = plt.subplots()
    D_highres = librosa.stft(y, hop_length=90, n_fft=6048)
    S_db_hr = librosa.amplitude_to_db(np.abs(D_highres), ref=np.max)
    img = librosa.display.specshow(S_db_hr, hop_length=hop_length,
                                   ax=ax)
    fig.colorbar(img, ax=ax, format="%+2.f dB")
    plt.axis('off')
    plt.colorbar
    plt.savefig('test_slika.png')
    label = PhotoImage(file=test_slika )
    image_label= Label(image=label,width=360,height=280)
    image_label.place(x=580,y=200)
    img = cv2.imread("test_slika.png")
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE), 3)
    img = img.reshape(-1, IMG_SIZE, IMG_SIZE, 3)
    prediction = model.predict(img)
    printed = print(CATEGORIES[int(prediction[0][0])])
    text_label = Label(window, text = (CATEGORIES[int(prediction[0][0])]))
    text_label.config(font=("Courier", 44))
    text_label.config(bg="white")
    text_label.place(x=580,y=500)

def klikslika():
    global adar_label
    global adar
    global img, text_label, printed
    window.pozslike = filedialog.askopenfilename(initialdir="Data/images_original", title="Odaberi sliku",filetypes=(("png files", "*.png"), ("jpeg files", "*.jpeg")))
    adar = PhotoImage(file=window.pozslike)
    adar_label = Label(image=adar)
    adar_label.place(x=0, y=200)
    img = cv2.imread(window.pozslike)
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE), 3)
    img = img.reshape(-1, IMG_SIZE, IMG_SIZE, 3)
    prediction = model.predict(img)
    printed = print(CATEGORIES[int(prediction[0][0])])
    print(prediction)
    text_label = Label(window, text = (CATEGORIES[int(prediction[0][0])]))
    text_label.config(font=("Courier", 44))
    text_label.config(bg="white")
    text_label.place(x=10,y=500)


tipkaslika = Button(window, text="Slika",height=5,width=20, command=klikslika)
tipkaslika.place(x=100,y=50)

tipkazvuk = Button(window, text="Zvuk",height=5,width=20, command=klikzvuk)
tipkazvuk.place(x=600,y=50)




img = cv2.imread('test_slika.png')
img = cv2.resize(img, (IMG_SIZE, IMG_SIZE), 3)
img = img.reshape(-1, IMG_SIZE, IMG_SIZE, 3)



#Data/images_original/classical/classical00026.png#
#Data/images_original/classical/classical00009.png#


window.mainloop()